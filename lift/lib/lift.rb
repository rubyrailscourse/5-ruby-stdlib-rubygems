require 'logger'
require_relative 'lift/main'
require_relative 'lift/log'

module Lift

  def self.create
    Main.new
  end

  def self.version
    '1.0.0'
  end
end

lift = Lift.create
lift.load! 40
lift.load! 140
lift.load! 100
lift.move 7
lift.release! 80
lift.move 7
lift.move 7
lift.move 54
lift.release! 400
lift.move 4
lift.release! 200
lift.move 4
