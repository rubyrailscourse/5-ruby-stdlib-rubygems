require_relative 'log'

module Lift
  module Validator

    MAX_WEIGHT = 250
    FLOOR_RANGE = (1..25)

    attr_reader :error

    class FloorRangeError < StandardError
    end

    class FloorCurrentError < StandardError
    end

    class WeightError < StandardError
    end

    def valid? *args
      clear_error!
      args.each do |validator_name|
        self.send "check_#{validator_name}!"
      end
    rescue FloorRangeError, FloorCurrentError, WeightError => error
    ensure
      @error = error
      @log.write_error(@error) if @error
      return @error.nil?
    end

    private

    def clear_error!
      @error = nil
    end

    def check_floor_range!
      unless @engine.floor.instance_of?(Fixnum) && FLOOR_RANGE.include?(@engine.floor)
        raise FloorRangeError, "The floor should be an Integer number between 1 and 25."
      end
    end

    def check_floor_current!
      if @engine.current_floor == @engine.floor
        raise FloorCurrentError, "You already on the #{@engine.floor} floor. Choose another floor or get out!"
      end
    end

    def check_weight!
      unless allowed_weight?
        raise WeightError, "Current weight (#{@weight} kg) is more than allowed (#{MAX_WEIGHT} kg). You will stay on the current floor."
      end
    end

    def allowed_weight?
      @weight <= MAX_WEIGHT
    end
  end
end
