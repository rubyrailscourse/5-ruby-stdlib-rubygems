module Lift
  class Log

    def initialize
      @session = Logger.new('data/logfile.log')
      @session.info("Program started")
    end

    def write_error(message)
      @session.error(message)
    end
  end
end
