module Lift
  class Engine

    attr_accessor :floor, :current_floor

    def initialize
      @current_floor = 1
    end

    def move
      @floor > @current_floor ? up : down
      set_current_floor!
    end

    private

    def up
      puts "Moving up..."
      @current_floor.upto(@floor) do |i|
        puts "#{i} floor"
      end
    end

    def down
      puts "Moving down..."
      @current_floor.downto(@floor) do |i|
        puts "#{i} floor"
      end
    end

    def set_current_floor!
      @current_floor = @floor
    end
  end
end
