require_relative 'engine'
require_relative 'validator'

module Lift
  class Main
    include Validator

    def initialize
      @weight = 0
      @engine = Engine.new
      @log = Log.new
    end

    def move floor
      @engine.floor = floor
      if valid? :floor_range, :floor_current, :weight
        @engine.move
      else
        puts @error
      end
    end

    def release! weight
      @weight = weight
      puts "[Release]: Current weight is #{@weight}"
    end

    def load! weight
      @weight += weight
      puts "[Load]: Current weight is #{@weight}"
    end
  end
end
