# Используйте блоки
class Mothership
  def compute
    right_ascension = rand(0..360)
    declension = rand(-90..90)
    [right_ascension, declension]
  end
end

class Ship
  def jump &blk
    puts "Jump to #{blk.call} position"
  end
end

orion = Ship.new
sirius = Mothership.new
orion.jump{sirius.compute}
